
-----------
== INDEX ==
-----------

1. What is this?
2. How do I install it?
3. How do I use it?

A. Getting GNAT

----------------------
== 1. What is this? ==
----------------------

This is a program to create "animations" from a pair of SVG files.  It
takes two SVG files that differ by some object "transformed" (i.e.,
rotated, translated, scaled or a combination of them) and creates some
intermediate files by "interpolating" the transforms.

For example, say that you transformed a text by translating it by
rotating it by 45 degrees, if you ask for 10
intermediate images, every image will differ by the previous one for a
rotation by 4.5 degrees.  The objects to be "animated" are identified
by their attribute "id"

-----------------------------
== 2. How do I install it? ==
-----------------------------

The program is written in Ada, so you need to compile it.  The
following instructions are relative to the GNAT compiler (a gcc
frontend) available at http://libre.adacore.com/ . (See Appendix A for
some details about downloading GNAT).

Supposing you have GNAT installed

1. Unzip the archive (you already did this step, if you are reading this)

2. Go to the "root" of the decompressed archive.  You should see few
   directories (src, doc, bin, ...) and a file svg_animator.gpr

3. From the command line give the command

              gprbuild

   It should select svg_animator.gpr by default.  

(Yes, no configure step... Ada is quite portable...)

4. You should find in the directory bin/ the executable 

      svg_animator-main      (if you are in Linux)
      svg_animator-main.exe  (if you are in Windows)


----------------------
== How do I use it? ==
----------------------

It is quite simple.  Suppose you want 9 intermediate images between
the first and the last.  Suppose also that you want to animate two
objects whose ids are "foo" and "bar" (a lot of fantasy, right?).

Just save your image in a filename like foo_test-00.svg and the last
image in foo_test-10.svg.  (You can change the filename, as long as
they differ only for the number, it is not even necessary that the
numbers have the same "length", the first file can be foo_test-0.svg)

Now call

   svg_animator-main foo_test-00.svg foo_test-10.svg foo,bar

Note that the ids of the objects to be animated are separated by
commas.  You'll find in the current working directory the
files foo_test-01.svg, foo_test-02.svg, ..., foo_test-09.svg.  You can
change the destination directory with the option --output-dir, for
example with

   svg_animator-main --output-dir=/tmp foo_test-00.svg foo_test-10.svg foo,bar

the files foo_test-01.svg, foo_test-02.svg, ..., foo_test-09.svg will
be written in /tmp and not in the current dir.

To be precise: the program expects the two filenames to be the
concatenation of a "head", a "number" and a "tail," with both files
with the same head an tail. For example, in the case above the head
was "foo_test-" and the tail was ".svg".  The lengths of the "number"
parts do not need to be the same, but if they are, the a fixed length
number (left padded with zeros) is used for all the intermediate
files, otherwise the number field of the intermediate files will have
its "natural" length.




------------------------------------------------------------------------

---------------------
== A. Getting GNAT == 
---------------------

Getting the Ada compiler GNAT is not difficult, but the first time it
could not be clear the parts that are needed.  This is a brief guide
about it (since you are planning to compile the program, I guess you
are not totally naive in computers...)

1. Go to http://libre.adacore.com/

2. You'll see a page that asks you if you want the GPL version or the
   Pro one.  GNAT has a double-license model, with a free version for
   personal and open source development, and a Pro version for commercial
   development.  Select the GPL version.

3. You'll land on a page where they ask you your e-mail if you are
   interested in getting news. It is not mandatory, so you can skip
   it.  At the end of the page you'll find a link to "create your
   download".

4. Follow that link and you will land in a page where you can choose
   your architecture (Linux, Mac, Windows, ...) and where you have a
   list of downloadable things.  This is maybe the most confusing
   part, since you could be unsure about what to download.

   You need only the first entry labeled with "GNAT <year>" or
   something like this.  Selecting that entry, you can choose a README
   file, an archive (or an excutable, depending on your architecture)
   and a sub-entry "Sources" (or something similar).  In order to
   compile this program, you need only the archive/executable, you do
   not need the sources, nor any of the other "packages" (although
   some of them are quite neat for an Ada programmer... ;-)

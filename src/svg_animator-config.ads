with Ada.Containers.Indefinite_Vectors;

--  This package parses the command line and export to the rest
--  of the code the results of said parsing via functions and
--  variables.
package SVG_Animator.Config is

   type Frame_Index is new Positive;
   type Attribute_Name_Type is new String;
   type Object_ID_Type is new String;
   type Object_Index is range 1 .. 128;

   No_ID : constant Object_ID_Type := "";

   procedure Initialize;
   --  To be called at the beginning of the program.  Why relying on this
   --  "primitive" approach and not on the initialization code written
   --  in the body?  Because if there are some error in the parsing,
   --  the exception Bad_Command_Line can be raised and caught by the main,
   --  if the parsing was done in the initialization code, the exception
   --  could not be caught.

   function First_Frame_Filename return String;
   --  The name of the file with the first frame

   function Last_Frame_Filename return String;
   --  The name of the file with the last frame

   function First_Frame_Index return Frame_Index;
   --  The time index of the first frame

   function Last_Frame_Index return Frame_Index;
   --  The time index of the last frame

   function Nth_Frame_Filename (N : Frame_Index) return String;
   --  The filename to be used to save the n-th frame

   function N_Objects return Natural;
   --  Number of objects to be animated

   function First_Object_Index return Object_Index
     with Pre => N_Objects > 0;

   function Last_Object_Index return Object_Index
     with Pre => N_Objects > 0;

   function Id_Label return Attribute_Name_Type;
   --  The name of the attribute to be used as id.

   function Nth_Object_Id (N : Object_Index) return Object_ID_Type
     with Pre =>
       N <= Last_Object_Index
       and N >= First_Object_Index;
   --  The id (that is, the value of the attribute whose name is
   --  returned by ID_Label) of the n-th object to be animated


   function Keep_Clip_Still (N : Object_Index) return Boolean
     with
       Pre => N <= Last_Object_Index
       and N >= First_Object_Index;

   function Help_Line return String;

   function Verbose return Boolean;


   package Object_ID_Vectors is
     new Ada.Containers.Indefinite_Vectors (Index_Type   => Object_Index,
                                            Element_Type => Object_ID_Type);

   procedure Set_Object_Ids (Names : Object_ID_Vectors.Vector)
     with
       Pre => N_Objects = 0 and Natural(Names.Length) > 0,
       Post => N_Objects = Natural (Names.Length)
     and First_Object_Index = Names.First_Index
     and Last_Object_Index = Names.Last_Index
     and (for all I in First_Object_Index .. Last_Object_Index
            => Names (I) = Nth_Object_Id (I));

   function Debug return Boolean
   is (False);
   -- Placeholder function to be implemented maybe in a future.
end SVG_Animator.Config;

with Input_Sources.File; use Input_Sources.File;
with Sax.Readers; use Sax.Readers;
with DOM.Readers; use DOM.Readers;
with DOM.Core; use DOM.Core;
with DOM.Core.Documents; use DOM.Core.Documents;
with DOM.Core.Nodes; use DOM.Core.Nodes;
with DOM.Core.Elements; use DOM.Core.Elements;

with Ada.Command_Line;   use Ada.Command_Line;
with Ada.Text_IO; use Ada.Text_IO;

with SVG_Animator.Transforms;          use SVG_Animator.Transforms;

procedure Svg_Animator.Main is
   Input  : File_Input;
   Reader : Tree_Reader;
   Doc    : Document;
   List   : Node_List;
   N      : Node;
begin
   if Argument_Count < 5 then
      Put_Line (Standard_Error,
                "Usage: SVG_Animator input output_pattern id rot n_step");

      Set_Exit_Status (Failure);
      return;
   end if;

   declare
      Input_Filename    : constant String := Argument (1);
      Output_Format     : constant String := Argument (2);
      Object_ID         : constant String := Argument (3);
      rotation_angle    : constant Degree := Degree'Value (Argument (4));
      N_Steps           : constant Natural := Natural'Value (Argument (5));
      Initial_Transform : SVG_Transform;
      step_transform    : constant SVG_Transform := Rotation (rotation_angle);
   begin
      Set_Public_Id (Input, "Preferences file");
      Open (Input_Filename, Input);

      Set_Feature (Reader, Validation_Feature, False);
      Set_Feature (Reader, Namespace_Feature, False);

      Parse (Reader, Input);
      Close (Input);

      Doc := Get_Tree (Reader);

      List := Get_Elements_By_Tag_Name (Doc      => Doc,
                                        Tag_Name => "*");

      for I in 1 .. Length (List) loop
         N := Item (List, I - 1);

         if Object_ID = Get_Attribute (Elem => n, Name => "id") then
            Put_Line (Standard_Error, "found! transform='" & Get_Attribute (N, "transform") & "'");

            if Get_Attribute (N, "transform") /= "" then
               Put_Line (Standard_Error, "has a transform");
               Initial_Transform := Value (Get_Attribute (N, "transform"));
            else
               Put_Line (Standard_Error, "has not a transform");
               Initial_Transform := Identity;
            end if;

            put_line (Standard_Error, "nuova : " & image (Initial_Transform * step_transform));

            Set_Attribute (n, "transform", image (Step_Transform* Initial_Transform));
         end if;
      end loop;

      print (doc,EOL_Sequence => "" & Character'val(10));

      Free (Reader);
   end;
end Svg_Animator.Main;

private
package SVG_Animator.Transforms.Parser is
   type Transforms_Class is (Rotation, Translation, Scale, Matrix);

   type Transforms_Descriptor (Class : Transforms_Class) is
      record
         case Class is
            when Rotation =>
               Angle : Degree;
               Center : Point;

            when Translation =>
               X, Y  : Coordinate;

            when Scale =>
               Rho_X, Rho_Y : SVG_Float;

            when Matrix =>
               Data  : Full_Transform;
         end case;
      end record;

   function Parse (Input : String) return Transforms_Descriptor;

   Parsing_Error : exception;
end SVG_Animator.Transforms.Parser;

pragma Ada_2012;

with Tokenize;
with Ada.Strings.Fixed;
with Ada.Command_Line;    use Ada.Command_Line;

package body CLI_Parsing is
   Current_Arg : Positive := 1;

   -------------------
   -- Remaining_Arg --
   -------------------

   function Remaining_Arg return Natural
   is (Argument_Count - Current_Arg + 1);

   -------------------
   -- Next_Argument --
   -------------------

   function Next_Argument return String
   is
   begin
      Current_Arg := Current_Arg + 1;
      return Argument (Current_Arg - 1);
   end Next_Argument;

   -------------------
   -- Next_Argument --
   -------------------

   function Next_Argument return Unbounded_String
   is (To_Unbounded_String (Next_Argument));


   ----------
   -- Peek --
   ----------

   function Peek return String
   is (Argument (Current_Arg));

   -------------------
   -- Skip_Argument --
   -------------------

   procedure Skip_Argument
   is
   begin
      Current_Arg := Current_Arg + 1;
   end Skip_Argument;

   -----------------------
   -- Is_End_Of_Options --
   -----------------------

   function Is_End_Of_Options (X : String) return Boolean
   is (X = "--");

   ---------------
   -- Is_Option --
   ---------------

   function Is_Option (X : String) return Boolean
   is (X'Length > 2
       and then X (X'First .. X'First + 1) = "--");



   -----------------------
   -- Option_Processing --
   -----------------------

   package body Option_Processing is
      -----------
      -- Parse --
      -----------

      function Parse
        (Item : String; Kind : Parameter_Kind) return Option_Descriptor
      is
         use Tokenize;
         use Ada.Strings;
         use Ada.Strings.Fixed;

         Names : constant Token_Array := Split (Item, ',');
         Result : Option_Descriptor :=
                    Option_Descriptor'(Set  => Option_Name_Sets.Empty_Set,
                                       Kind => Kind);
      begin
         for Name of Names loop
            declare
               Name_Trimmed : constant String := Trim (To_String (Name), Both);
            begin
               Result.Set.Insert (Option_Name (Name_Trimmed));
            end;
         end loop;

         return Result;
      end Parse;

      -------------
      -- Process --
      -------------

      procedure Process
        (Options  : Descriptor_Array;
         Callback : Callback_Access)
      is
         procedure Process_Option (Option : String)
         is
            use Ada.Strings.Fixed;

            Equal_Pos : constant Natural := Index (Option, "=");
            Has_Value : constant Boolean := Equal_Pos /= 0;

            Name      : constant String :=
                          (if Has_Value
                           then
                              Option (Option'First + 2 .. Equal_Pos - 1)
                           else
                              Option (Option'First + 2 .. Option'Last));

            Value     : constant String :=
                          (if Has_Value
                           then
                              Option (Equal_Pos + 1 .. Option'Last)
                           else
                              "");
         begin
            for Opt in Option_Type loop
               if Options (Opt).Set.Contains (Option_Name (Name)) then

                  case Options (Opt).Kind is
                     when Optional =>
                        null;

                     when Mandatory =>
                        if not Has_Value then
                           raise Constraint_Error;
                        end if;

                     when Prohibited =>
                        if Has_Value then
                           raise Constraint_Error;
                        end if;
                  end case;

                  Callback (Option        => Opt,
                            Has_Parameter => Has_Value,
                            Value         => Value,
                            Name          => Name);

                  return;
               end if;
            end loop;

            raise Constraint_Error with "Option '" & Name & "' Unknown";
         end Process_Option;
      begin
         while Remaining_Arg > 0 and then Is_Option (Peek) loop
            Process_Option (Next_Argument);
         end loop;

         if Is_End_Of_Options (Peek) then
            Skip_Argument;
         end if;
      end Process;

   end Option_Processing;

end CLI_Parsing;

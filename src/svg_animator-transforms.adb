with Ada.Strings.Unbounded;
with Ada.Strings.Fixed;

with Ada.Numerics.Generic_Elementary_Functions;
with Ada.Text_IO;
use Ada.Text_IO;

with Svg_Animator.Transforms.Parser;

package body Svg_Animator.Transforms is
   package SVG_Float_IO is
     new Ada.Text_IO.Float_IO (SVG_Float);

   package SVG_Elementary_Functions is
     new Ada.Numerics.Generic_Elementary_Functions (SVG_Float);

   Identity_Transform : constant Linear_Transform := ((1.0, 0.0),
                                            (0.0, 1.0));

   --     Identity_Transform : constant SVG_Transform := (Linear => ((1.0, 0.0),
--                                                                (0.0, 1.0)),
--                                                     Trans  => (0.0, 0.0));
   --------------
   -- Identity --
   --------------

   function Identity return SVG_Transform is
   begin
      return (Linear => Identity_Transform, Trans => (0.0, 0.0));
   end Identity;

   --------------
   -- Rotation --
   --------------

   function Rotation (X : Degree) return SVG_Transform is
      Rad : constant SVG_Float := SVG_Float (X) * Ada.Numerics.Pi / 180.0;
      C   : constant SVG_Float := SVG_Elementary_Functions.Cos (Rad);
      S   : constant SVG_Float := SVG_Elementary_Functions.Sin (Rad);
   begin
      return (Linear => ((C, S),
                         (-S, C)),
              Trans  => (0.0, 0.0));
   end Rotation;

   function Rotation (Angle : Degree; Center : Point) return SVG_Transform is
   begin
      return Translation (-Center) * Rotation (Angle) * Translation (Center);
   end Rotation;

   function Inv (X : SVG_Transform) return SVG_Transform is
      Tmp : constant Linear_Transform := Inverse (X.Linear);
   begin
      return (Linear => tmp,
              Trans  => -Tmp * X.Trans);
   end Inv;


   -----------------
   -- Translation --
   -----------------



   function Translation (X, Y : Coordinate) return SVG_Transform is
   begin
      return (Linear => Identity_Transform,
              Trans  => Point'(SVG_Float (X), SVG_Float (Y)));
   end Translation;

   -----------
   -- Scale --
   -----------


   function Scale (Rho_X, Rho_Y : SVG_Float) return SVG_Transform is
   begin
      return (Linear => ((Rho_X, 0.0),
                         (0.0, Rho_Y)),
              Trans  => (0.0, 0.0));
   end Scale;



   -----------
   -- Image --
   -----------

   function Image (X : SVG_Transform) return String is
      use Ada.Strings.Unbounded;

      Result : Unbounded_String;

      function Image (X : SVG_Float) return String is
         use Ada.Strings, Ada.Strings.Fixed;

         Buffer : String (1 .. 16);
      begin
         SVG_Float_IO.Put (To   => Buffer,
                           Item => X,
                           Aft  => 8,
                           Exp  => 0);

         return Trim(Buffer, Both);
      end Image;
   begin
      for Col in X.Linear'Range (2) loop
         for Row in X.Linear'Range(1) loop
            Result := Result & Image (X.Linear (Row, Col)) & ",";
         end loop;
      end loop;

      Result := Result  & Image (SVG_Float (X.Trans (1))) & "," & Image (SVG_Float (X.Trans (2)));

      return "matrix(" & To_String(Result) & ")";
   end Image;

   -----------
   -- Value --
   -----------

   function Value (X : String) return SVG_Transform is
      Parsed : constant Parser.Transforms_Descriptor := Parser.Parse (X);
   begin
      case Parsed.Class is
         when Parser.Rotation =>
            return Rotation (Parsed.Angle);

         when Parser.Translation =>
            return Translation (Parsed.X, Parsed.Y);

         when Parser.Scale =>
            return Scale (Parsed.Rho_X, Parsed.Rho_Y);

         when Parser.Matrix =>
            return Matrix (Parsed.Data);
      end case;
   end Value;

   function Nth_Root (X : SVG_Transform; N : Positive) return SVG_Transform
   is

      function Nth_Root (X : Linear_Transform; N : Positive) return Linear_Transform
      is
         use SVG_Elementary_Functions;

         Norm : SVG_Float;
         Normalized : Linear_Transform;
         Angle      : SVG_Float;
         S, C       : SVG_Float;
      begin
         if Is_Scaled_Rotation (X) then
            Norm := Sqrt (X (1, 1) ** 2 + X (1, 2) ** 2);
            Normalized := X / Norm;

            Angle := Arctan (Normalized (2, 1), Normalized (1, 1)) / SVG_Float (N);

--           Put_Line (Standard_Error, "normalized=" & SVG_Float'Image (Normalized (1, 1)));
--           Put_Line (Standard_Error, "norm=" & SVG_Float'Image (Norm));
--           Put_Line (Standard_Error, "alfa=" & SVG_Float'Image (Angle) & ","
--                     & SVG_Float'Image(Arctan (Normalized (2, 1), Normalized (1, 1))));

            C := Cos (Angle);
            S := Sin (Angle);

            return (Norm ** (1.0 / SVG_Float (N))) * ((C, -S), (S, C));

         elsif Is_Diagonal (X) then
            if X (1, 1) < 0.0 or X (2, 2) < 0.0 then
               raise Constraint_Error
                 with "Cannot compute Nth roots of negative diagonal matrices";
            end if;

            return ((X (1, 1) ** (1.0 / SVG_Float (N)), 0.0),
                    (0.0, X (2, 2) ** (1.0 / SVG_Float (N))));

         else
            raise Constraint_Error
              with "Cannot compute Nth root of matrix";
         end if;
      end Nth_Root;

      function Nth_Root_Translation (A : Linear_Transform;
                                     X : Point;
                                     N : Natural)
                                     return point
      is
         B : Linear_Transform := Identity_Transform;
      begin

         for I in 1 .. N - 1 loop
            -- Here B = A^(I-1) + ... + I
            B := A * B + Identity_Transform;

            -- Here B = A^I + ... + I
         end loop;

         return Inverse (B) * X;
      end Nth_Root_Translation;

      Tmp : constant Linear_Transform := Nth_Root (X.Linear, N);
      Result : SVG_Transform;
   begin
      Result := (Linear => Tmp,
                 Trans  => Nth_Root_Translation (Tmp, X.Trans, N));

      --  Put_Line (Standard_Error, "d=" & SVG_Float'Image (Distance (Result ** N, X)));
      --  Put_Line (Standard_Error, Image (Result));
      --  Put_Line (Standard_Error, Image (Result ** N));
      --  Put_Line (Standard_Error, Image (X));
      return Result;
   end Nth_Root;

   function "**" (X : SVG_Transform; N : Natural) return SVG_Transform
   is
      Result : SVG_Transform := Identity;
   begin
      -- Slow approach, yes I know...
      for I in 1 .. N loop
         Result := Result * X;
      end loop;

      return Result;
   end "**";

   function Distance (X, Y : SVG_Transform) return SVG_Float is
      Result : SVG_Float;
   begin
      Result := 0.0;
      for Row in X.Linear'Range (1) loop
         for Col in X.Linear'Range (2) loop
            Result := Result + abs (X.Linear (Row, Col) - Y.Linear (Row, Col));
         end loop;
      end loop;

      Result := Result + abs (X.Trans - Y.Trans);

      return Result;
   end Distance;

   function "*" (X, Y : SVG_Transform) return SVG_Transform
   is
   begin
      return SVG_Transform'(Linear => X.Linear * Y.Linear,
                            Trans  => X.Linear * Y.Trans + X.Trans);
   end "*";


end Svg_Animator.Transforms;

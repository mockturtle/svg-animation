with Generic_Scanner;
with Ada.Unchecked_Deallocation;
with Ada.Text_IO; use Ada.Text_IO;

package body SVG_Animator.Transforms.Scanner is
   package Basic_Scanners is
     new Generic_Scanner (Token_Type => Token_Class);

   Regexps : constant Basic_Scanners.Token_Regexp_Array :=
               (Id     => Basic_Scanners.ID_Regexp,
                Number => Basic_Scanners.Float_Regexp,
                Open   => Basic_Scanners.Constant_String ("("),
                Close  => Basic_Scanners.Constant_String (")"),
                Comma  => Basic_Scanners.Constant_String (","),
                EOF    => Basic_Scanners.EOF_Pseudo_Regexp);

   Basic_Scanner : Basic_Scanners.Scanner_Access := null;

   Current_Token : Basic_Scanners.Full_Token_Type;
   Current_Token_Used  : Boolean;

   procedure Update_Token (Just_Peeking : Boolean) is
   begin
--        Put_Line (Standard_Error, "in update token");

      if Current_Token_Used then
--           Put_Line (Standard_Error, "Calling scan");

         if Basic_Scanner.Used then
            Basic_Scanner.Next;
         end if;

         Current_Token := Basic_Scanner.Full_Token;
      end if;

      Current_Token_Used := not Just_Peeking;
   end Update_Token;

   ----------
   -- Init --
   ----------

   procedure Init (Input : String) is
      procedure Free is new Ada.Unchecked_Deallocation
        (Object => Basic_Scanners.Scanner_Type,
         Name   => Basic_Scanners.Scanner_Access);

      use type Basic_Scanners.Scanner_Access;
   begin
      if Basic_Scanner /= null then
         Free (Basic_Scanner);
      end if;

      Basic_Scanner := Basic_Scanners.New_Scanner (Input         => Input,
                                                   Regexps       => Regexps,
                                                   Scan          => True);

      Current_Token_Used := True;
      Basic_Scanner.Tracing (On => False);
   end Init;

   ----------------
   -- Next_Token --
   ----------------

   function Next_Token return Token_Type is
   begin
      if Basic_Scanner.At_EOF then
         return Token_Type'(Class => EOF);
      end if;

      Update_Token (Just_Peeking => False);

      case Current_Token.Token is
         when Id =>
            return Token_Type'(Class => Id,
                               Name  => Current_Token.Value);

         when Number =>
            return (Class => Number,
                    Value => SVG_Float'Value (To_String (Current_Token.Value)));

         when  Open =>
            return (Class => Open);

         when Close =>
            return (Class => Close);

         when Comma =>
            return (Class => Comma);

         when EOF =>
            return (Class => EOF);
      end case;
   end Next_Token;

   ----------------
   -- Peek_Class --
   ----------------

   function Peek_Class return Token_Class is
   begin
      Update_Token (Just_Peeking => True);
      return Current_Token.Token;
   end Peek_Class;

end SVG_Animator.Transforms.Scanner;

with DOM.Core; use DOM.Core;

with SVG_Animator.Config;
with SVG_Animator.Transforms;
with DOM.Readers;

package SVG_Animator.XML_Utilities is
   use type Config.Object_ID_Type;

   type SVG_File is private;

   function Load (Filename : String) return SVG_File;

   procedure Save (Item : SVG_File; Filename : String);

   function Exists (Frame : SVG_File;
                    ID    : Config.Object_ID_Type;
                    Label : Config.Attribute_Name_Type := "id")
                    return Boolean;

   function Get_Root_Attribute (Frame     : SVG_File;
                                Attribute : String)
                                return String;

   function Get_Transform (Frame : SVG_File;
                           ID    : Config.Object_ID_Type;
                           Label : Config.Attribute_Name_Type)
                           return Transforms.SVG_Transform
     with Pre => Id /= Config.No_ID;

   function Clip_Path_Id (Frame : SVG_File;
                          ID    : Config.Object_ID_Type;
                          Label : Config.Attribute_Name_Type)
                          return Config.Object_ID_Type
     with Pre => Id /= Config.No_ID;


   procedure Set_Transform (Frame :  SVG_File;
                            ID    : Config.Object_ID_Type;
                            Label : Config.Attribute_Name_Type;
                            Value : Transforms.SVG_Transform)
     with Pre => Id /= Config.No_ID;

   Node_Not_Found : exception;
   Duplicated_ID  : exception;
   Bad_Clip_Path_Url : exception;
private
   type SVG_File is
      record
         Reader : DOM.Readers.Tree_Reader;
         Doc    : Document;
      end record;

end SVG_Animator.XML_Utilities;

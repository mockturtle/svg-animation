with Ada.Exceptions;     use Ada.Exceptions;
with Ada.Command_Line;   use Ada.Command_Line;
with Ada.Text_IO; use Ada.Text_IO;
with Ada.Strings.Unbounded;
with Ada.Strings.Fixed;

with SVG_Animator.Transforms;          use SVG_Animator.Transforms;
with SVG_Animator.Config;
with SVG_Animator.XML_Utilities;

with Tokenize;

procedure Svg_Animator.Main is
   use type Config.Frame_Index;

   First_Frame       : XML_Utilities.SVG_File;
   Last_Frame        : XML_Utilities.SVG_File;
   Working_Frame     : XML_Utilities.SVG_File;
   First_Transform   : SVG_Transform;
   Last_Transform    : SVG_Transform;

   function Extract_Objects (X, Y : XML_Utilities.SVG_File)
                             return Config.Object_ID_Vectors.Vector
   is
      function Extract_From_Root (From : XML_Utilities.SVG_File)
                                  return Config.Object_ID_Vectors.Vector
      is
         Mobile_Ids : constant String :=
                        XML_Utilities.Get_Root_Attribute (From, "mobile");
      begin
         if Mobile_Ids = "" then
            return Config.Object_ID_Vectors.Empty_Vector;
         else
            declare
               use Tokenize;
               use Ada.Strings;
               use Ada.Strings.Unbounded;

               Result : Config.Object_ID_Vectors.Vector;

               IDs : constant Tokenize.Token_Array :=
                       Split (To_Be_Splitted => Mobile_Ids,
                              Separator      => ',');
            begin
               for Name of Ids loop
                  Result.Append
                    (Config.Object_ID_Type
                       (To_String (Trim (Name, Both))));
               end loop;

               return Result;
            end;
         end if;
      end Extract_From_Root;

      function Extract_Indexed_Names (From       : XML_Utilities.SVG_File;
                                       Name_Radix : Config.Object_ID_Type)
                                       return Config.Object_ID_Vectors.Vector
      is
         use Config;
         use Ada.Strings;
         use Ada.Strings.Fixed;

         Result : Object_ID_Vectors.Vector;
      begin
         for Idx in Object_Index loop
            declare
               Num : constant Object_ID_Type :=
                       Object_ID_Type (Trim (Object_Index'Image (Idx), Both));

               ID  : constant Config.Object_ID_Type := Name_Radix & Num;
            begin
               if Config.Debug then
                  Put_Line ("(" & String (Id) & ") "
                            & Boolean'Image (XML_Utilities.Exists (From, ID)));
               end if;

               exit when not XML_Utilities.Exists (From, ID);

               Result.Append (Id);
            end;
         end loop;

         return Result;
      end Extract_Indexed_Names;

      function Extract_Defaults (From : XML_Utilities.SVG_File)
                                 return Config.Object_ID_Vectors.Vector
      is (Extract_Indexed_Names (From, "anim"));

      function Extract (From : XML_Utilities.SVG_File)
                         return Config.Object_ID_Vectors.Vector
      is
         Result : Config.Object_ID_Vectors.Vector;
      begin
         Result := Extract_From_Root (From);

         if Result.Is_Empty then
            return Extract_Defaults (From);
         else
            return Result;
         end if;
      end Extract;
      A      : constant Config.Object_ID_Vectors.Vector := Extract (From => X);
      B      : constant Config.Object_ID_Vectors.Vector := Extract (From => Y);
      Result : Config.Object_ID_Vectors.Vector;

      use type Config.Object_ID_Type;
   begin
      --
      -- Yes, not very efficient, I know.  Complexity is quadratic, I
      -- know, but...
      --
      -- 1) This is executed only at start-up time and
      -- 2) The number of entries in A and B is expected to be small,
      --    just few units
      --
      for ID of A loop
         for J of B loop
            if ID = J then
               Result.Append (ID);
            end if;
         end loop;
      end loop;

      if Config.Verbose and not Result.Is_Empty then
         Put (Standard_Error, "Names taken from SVG files: ");

         for Name of Result loop
            Put (Standard_Error, " '" & String (Name) & "'");
         end loop;

         New_Line (Standard_Error);
      end if;

      return Result;
   end Extract_Objects;
begin
   Config.Initialize;

   --     Put_Line (Standard_Error, "from : " & Config.First_Frame_Filename);
   --     Put_Line (Standard_Error, "to: " & Config.Last_Frame_Filename);
   --     Put_Line (Standard_Error, "quarto:" & Config.Nth_Frame_Filename (4));
   --     Put_Line (Standard_Error, "primo, ultimo:"
   --               & Config.Frame_Index'Image (Config.First_Frame_Index)
   --               & Config.Frame_Index'Image (Config.Last_Frame_Index));

   First_Frame := XML_Utilities.Load (Config.First_Frame_Filename);
   Last_Frame := XML_Utilities.Load (Config.Last_Frame_Filename);

   if Config.N_Objects = 0 then
      declare
         To_Be_Animated : constant Config.Object_ID_Vectors.Vector :=
                            Extract_Objects (First_Frame, Last_Frame);
      begin
         if To_Be_Animated.Is_Empty then
            raise Bad_Command_Line
              with "No object IDs, neither on CLI, nor in files";
         end if;

         Config.Set_Object_Ids (To_Be_Animated);
      end;
   end if;



   Working_Frame := First_Frame;

   declare
      subtype Object_Index is
        Config.Object_Index
      range Config.First_Object_Index .. Config.Last_Object_Index;

      type Transform_Array is array (Object_Index) of SVG_Transform;
      type Flag_Array is array (Object_Index) of Boolean;

      Step_Transform    : Transform_Array;
      Current_Transform : Transform_Array;
      Clip_Transform    : Transform_Array;
      Object_Exists     : Flag_Array;
      N_Frames          : constant Integer :=
                            Integer (Config.Last_Frame_Index - Config.First_Frame_Index);
   begin
      for Obj in Object_Index loop
         Object_Exists (Obj) := True;

         begin
            -- Yes, I know, it is not nice to use exceptions
            -- for flow control, but I need a "fast and dirty"
            -- solutions.
            First_Transform := XML_Utilities.Get_Transform
              (Frame => First_Frame,
               ID    => Config.Nth_Object_Id (Obj),
               Label => Config.Id_Label);
         exception
            when XML_Utilities.Node_Not_Found =>
               Object_Exists (Obj) := False;

               Put_Line (Standard_Error,
                         "WARNING: Ignoring missing node "
                         & "'" & String (Config.Nth_Object_Id (Obj)) & "'");
         end;

         if Object_Exists (Obj) then
            First_Transform := XML_Utilities.Get_Transform
              (Frame => First_Frame,
               ID    => Config.Nth_Object_Id (Obj),
               Label => Config.Id_Label);

            Last_Transform := XML_Utilities.Get_Transform
              (Frame => Last_Frame,
               ID    => Config.Nth_Object_Id (Obj),
               Label => Config.Id_Label);

            if Config.Verbose then

               Put_Line (Standard_Error,
                         "Animating "
                         & "'" & String (Config.Nth_Object_Id (Obj)) & "'");

               Put_Line (Standard_Error, "From " & Image (First_Transform));
               Put_Line (Standard_Error, "To   " & Image (Last_Transform));
            end if;

            -- If I need to create N intermediate frames, the step must
            -- satisfy (step^(n+1)) * first = last, therefore
            -- step = sqrt(inv(first)*last, n+1)

            Step_Transform (Obj) :=
              Nth_Root (Last_Transform * Inv (First_Transform), N_Frames + 1);

            if Config.Verbose then
               Put_Line (Standard_Error, "Step " & Image (Step_Transform (Obj)));
            end if;

            Current_Transform (Obj) := First_Transform;

            declare
               use type Config.Object_ID_Type;

               Clip_Id : constant Config.Object_ID_Type :=
                           XML_Utilities.Clip_Path_Id
                             (Frame => First_Frame,
                              ID    => Config.Nth_Object_Id (Obj),
                              Label => Config.Id_Label);
            begin
               if Clip_Id = Config.No_ID then
                  Clip_Transform (Obj) := Identity;
               else
                  Clip_Transform (Obj) := XML_Utilities.Get_Transform
                    (Frame => First_Frame,
                     ID    => Clip_Id,
                     Label => Config.Id_Label);
               end if;
            end;
         end if;
      end loop;

      for Frame in Config.First_Frame_Index .. Config.Last_Frame_Index loop
         for Obj in Object_Index loop
            if Object_Exists (Obj) then
               Current_Transform (Obj) :=
                 Step_Transform (Obj) * Current_Transform (Obj);

               Clip_Transform (Obj) :=
                 Clip_Transform (Obj) * Inv (Step_Transform (Obj));


               --  Put_Line (Standard_Error,
               --            Obj'Img & ", "  & Frame'Img
               --            & "->"
               --            & Image (Current_Transform (Obj))
               --            & "; "
               --            & Image (Step_Transform (Obj)));
               --
               XML_Utilities.Set_Transform (Frame => Working_Frame,
                                            ID    => Config.Nth_Object_Id (Obj),
                                            Label => Config.Id_Label,
                                            Value => Current_Transform (Obj));

               declare
                  use type Config.Object_ID_Type;

                  Clip_Id : constant Config.Object_ID_Type :=
                              XML_Utilities.Clip_Path_Id
                                (Frame => First_Frame,
                                 ID    => Config.Nth_Object_Id (Obj),
                                 Label => Config.Id_Label);
               begin
                  --  Put_Line ("[" & String (Clip_Id) & "]");
                  --  Put_Line (Image (Current_Transform (Obj)));
                  --  Put_Line (Image (Clip_Transform (Obj)));
                  --
                  if Clip_Id /= Config.No_ID and Config.Keep_Clip_Still (Obj) then
                     --  Put_Line ("xxx");
                     XML_Utilities.Set_Transform
                       (Frame => Working_Frame,
                        ID    => Clip_Id,
                        Label => Config.Id_Label,
                        Value => Clip_Transform (Obj));
                  end if;
               end;
            end if;
         end loop;

         XML_Utilities.Save (Working_Frame, Config.Nth_Frame_Filename (Frame));
      end loop;
   end;
exception
   when E : Bad_Command_Line =>
      New_Line (Standard_Error);

      Put_Line (Standard_Error,
                "Bad command line: " & Exception_Message (E));

      New_Line (Standard_Error);

      Put_Line (Standard_Error, Config.Help_Line);

      Set_Exit_Status (Failure);

   when E : XML_Utilities.Node_Not_Found =>
      Put_Line (Standard_Error,
                "No node with ID='" & Exception_Message (E) & "'");

      Set_Exit_Status (Failure);

   when E : XML_Utilities.Duplicated_Id =>
      Put_Line (Standard_Error,
                "More than one node with ID='" & Exception_Message (E) & "'");

      Set_Exit_Status (Failure);
end Svg_Animator.Main;

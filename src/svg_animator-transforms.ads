with Ada.Numerics.Generic_Real_Arrays;

--  This package provides resources (types, functions, procedures...)
--  to handle the SVG transforms (creation of transforms, combinations,
--  powers, nth-roots...)
package Svg_Animator.Transforms is

   type SVG_Float is digits 8;

   package SVG_Matrices is
     new Ada.Numerics.Generic_Real_Arrays (SVG_Float);


   type Degree is new SVG_Float;
   type Coordinate is new SVG_Float;

   subtype Point is SVG_Matrices.Real_Vector (1 .. 2);
   subtype Linear_Transform is SVG_Matrices.Real_Matrix (1 .. 2, 1 .. 2);

   type Full_Transform is
      record
         Linear : Linear_Transform;
         Trans  : Point;
      end record;
   -- The type used to export a full (linear map + translation) SVG
   -- transform.

   type SVG_Transform is private;
   -- The internal representation of a transform

   function Matrix (Data : Full_Transform) return SVG_Transform;
   -- Convert a Full_Transform to an SVG_Transform

   function To_Descriptor (X : SVG_Transform) return Full_Transform;
   -- Convert an SVG_Transform to a Full_Transform

   function Image (X : SVG_Transform) return String;
   -- Create an SVG-compatible text representation of X

   function Value (X : String) return SVG_Transform;
   --  Parse a text representation of a transform

   function Identity return SVG_Transform;
   --  Return the identity transform

   function Rotation (X : Degree) return SVG_Transform;
   --  Return a  transform corresponding to a rotation around the origin

   function Rotation (Angle : Degree; Center : Point) return SVG_Transform;
   --  Return a  transform corresponding to a rotation around the given center

   function Translation (X, Y : Coordinate) return SVG_Transform;
   --  Return a  transform corresponding to a translation

   function Translation (T : Point) return SVG_Transform;
   --  Return a  transform corresponding to a translation

   function Scale (Rho : SVG_Float) return SVG_Transform;
   function Scale (Rho_X, Rho_Y : SVG_Float) return SVG_Transform;
   --  Return a  transform corresponding to a dilation

   function "*" (X, Y : SVG_Transform) return SVG_Transform;
   function "**" (X : SVG_Transform; N : Natural) return SVG_Transform;
   function "-" (P : Point) return Point;
   function Inv (X : SVG_Transform) return SVG_Transform;

   function Distance (X, Y : SVG_Transform) return SVG_Float;

   function Is_Admissible (X : SVG_Transform) return Boolean;
   -- Return true if we are able to compute a N-th roots of X

   function Nth_Root (X : SVG_Transform; N : Positive) return SVG_Transform
     with
       Pre => Is_Admissible(X),
       Post => Distance (Nth_Root'Result ** N, X) < 1.0e-5;

private
   use SVG_Matrices;

   Epsilon : constant SVG_Float := 1.0e-6;

   type SVG_Transform is new Full_Transform;

   function "-" (P : Point) return Point
   is ((-P (1), -P (2)));

   function Translation (T : Point) return SVG_Transform
   is (Translation (Coordinate (T (1)), Coordinate (T (2))));

   function Scale (Rho : SVG_Float) return SVG_Transform
   is (Scale (Rho, Rho));

   function To_Descriptor (X : SVG_Transform) return Full_Transform
   is (Full_Transform (X));

   function Matrix (Data : Full_Transform) return SVG_Transform
   is (SVG_Transform (Data));

   function Is_Diagonal (X : Linear_Transform) return Boolean
   is (abs (X (1, 2))+abs (X (2, 1)) <= Epsilon);
   -- Return true if X is diagonal matrix

   function Is_Scaled_Rotation (X : Linear_Transform) return Boolean
   is (abs (X (1, 1) - X (2, 2)) +
       abs (X (2, 1) + X (1, 2)) <= 2.0 * Epsilon);
   -- Return true if X is a rotation multiplied by a possible change of
   -- scale.  This means that X must be antisimmetric and with the
   -- values on the diagonal equal.

   function Is_Admissible (X : SVG_Transform) return Boolean
   is (Is_Scaled_Rotation (X.Linear) or Is_Diagonal (X.Linear));
end Svg_Animator.Transforms;

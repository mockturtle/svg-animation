with Ada.Command_Line;
with Ada.Characters.Handling;
with Ada.Strings.Unbounded;             use Ada.Strings.Unbounded;
with Ada.Strings.Fixed;                 use Ada.Strings.Fixed;
with SVG_Animator.Config.Die;
with Ada.Text_IO; use Ada.Text_IO;
pragma Warnings (Off, Ada.Text_IO);

with Ada.Directories.Hierarchical_File_Names;

package body SVG_Animator.Config is

   First_Filename      : Unbounded_String;
   Last_Filename       : Unbounded_String;
   Output_Pattern_Head : Unbounded_String;
   Output_Pattern_Tail : Unbounded_String;

   Variable_Size           : constant Natural := 0;
   Frame_Number_Field_Size : Natural;
   First_Frame_Number      : Frame_Index;
   Last_Frame_Number       : Frame_Index;


   Object_IDs    : Object_ID_Vectors.Vector;
   Object_Number : Object_Index;

   Object_ID_Label : Unbounded_String;

   Verbose_Flag    : Boolean := False;

   function Verbose return Boolean
   is (Verbose_Flag);

   procedure Create_Pattern (From        : Unbounded_String;
                             To          : Unbounded_String;
                             Head        : out Unbounded_String;
                             Tail        : out Unbounded_String;
                             Field_Size  : out Natural;
                             First_Frame : out Frame_Index;
                             Last_Frame  : out Frame_Index)
   is
      Shortest_Length      : constant Natural := Integer'Min (Length (From), Length (To));
      Head_End             : Natural;
      Tail_From_Start      : Natural;
      Tail_To_Start        : Natural;

      procedure Find_Tail (Item       : Unbounded_String;
                           From       : Positive;
                           Tail_Start : out Positive)
      is
         Not_Found : constant Positive := Length (Item) + 1;
      begin
         Tail_Start := Not_Found;
         for I in From .. Length (Item) loop
            if not (Element (Item, I) in '0' .. '9') then
               Tail_Start := I;
               exit;
            end if;
         end loop;

         if Tail_Start = From or Tail_Start = Not_Found then
            Die ("Could not extract output format");
         end if;
      end Find_Tail;
   begin

      Head_End := 0;
      for I in 1 .. Shortest_Length loop
         if Element (From, I) /= Element (To, I) then
            Head_End := I - 1;
            exit;
         end if;
      end loop;

      if Head_End = 0 then
         Die ("Could not find end of head");
      end if;

      Find_Tail (Item       => From,
                 From       => Head_End + 1,
                 Tail_Start => Tail_From_Start);

      Find_Tail (Item       => To,
                 From       => Head_End + 1,
                 Tail_Start => Tail_To_Start);

      if Slice (From, Tail_From_Start, Length (From)) /=
        Slice (To, Tail_To_Start, Length (To)) then
         Die ("Tails do not match");
      end if;

      Head := Unbounded_Slice (From, 1, Head_End);
      Tail := Unbounded_Slice (From, Tail_From_Start, Length (From));

      --        Put_Line (Standard_Error, "from, tail="
      --                  & Integer'Image (Length (From))
      --                  & Integer'Image (Length (tail)));
      if Length (From) /= Length (To) then
         Field_Size := Variable_Size;
      else
         Field_Size := Tail_From_Start - Head_End - 1;
      end if;

      --        Put_Line(Standard_Error, "field size=" & Field_Size'Img);

      First_Frame := Frame_Index'Value (Slice (From, Head_End + 1, Tail_From_Start - 1)) + 1;
      Last_Frame := Frame_Index'Value (Slice (To, Head_End + 1, Tail_To_Start - 1)) - 1;
   end Create_Pattern;

   procedure Parse_Object_Ids (Input : String)
   is

      procedure Clear_Object_Ids is
      begin
         Object_Number := Object_IDs.First_Index;
      end Clear_Object_Ids;

      procedure Append_To_Object_Ids (Item : String)
      is
      begin
         if Object_Number = Object_IDs.Last_Index then
            Die ("Too many IDs");
         end if;

         Object_IDs (Object_Number) := Object_ID_Type (Item);
         Object_Number := Object_Number + 1;
      end Append_To_Object_Ids;

      From : Natural := Input'First;
      To   : Natural;
   begin
      Clear_Object_Ids;

      loop
         To := Index (Source  => Input (From .. Input'Last),
                      Pattern => ",");

         exit when To = 0;

         if To > From + 1 then
            Append_To_Object_Ids (Input (From .. To - 1));
         end if;

         From := To + 1;

         exit when From > Input'Last;
      end loop;

      if From < Input'Last then
         Append_To_Object_Ids (Input (From .. Input'Last));
      end if;
   end Parse_Object_Ids;


   procedure Filenames_From_Radix (Radix : String)
   is
      use Ada.Directories;
      use Ada.Directories.Hierarchical_File_Names;

      function Make_Full (Name : String) return String
      is (if Is_Full_Name (Name) then
             Name
          else
             Compose (Containing_Directory => Current_Directory,
                      Name                 => Name,
                      Extension            => ""));


      Full_Radix   : constant String := Make_Full (Radix);
      Simple_Radix : constant String := Ada.Directories.Simple_Name (Full_Radix);
      Searched_Dir : constant String := Ada.Directories.Containing_Directory (Full_Radix);

      Extension   : constant String := "svg";

      Pattern     : constant String :=  Simple_Radix & "-*." & Extension;

      Head_Length : constant Natural := Simple_Radix'Length + 1;
      Tail_Length : constant Natural := Extension'Length + 1;

      type Index_Type is
         record
            Val   : Natural;
            Image : Unbounded_String;
         end record;

      No_Index : constant Index_Type := (0, Null_Unbounded_String);

      function Min (X, Y : Index_Type) return Index_Type
        with
          Pre => not (X = No_Index and Y = No_Index);

      function Max (X, Y : Index_Type) return Index_Type
        with
          Pre => not (X = No_Index and Y = No_Index);

      function Min (X, Y : Index_Type) return Index_Type
      is (if X = No_Index then
             Y
          elsif Y = No_Index then
             X
          elsif X.Val < Y.Val then
             X
          else
             Y);

      function Max (X, Y : Index_Type) return Index_Type
      is (if X = No_Index then
             Y
          elsif Y = No_Index then
             X
          elsif X.Val < Y.Val then
             Y
          else
             X);

      function Value (X : String) return Index_Type
      is (Index_Type'(Val   => Integer'Value (X),
                      Image => To_Unbounded_String (X)));

      function Image (X : Index_Type) return String
      is (To_String (X.Image) & ", " & Integer'Image (X.Val));
      pragma Unreferenced (Image);

      Min_Index : Index_Type := No_Index;
      Max_Index : Index_Type := No_Index;


      procedure Parse_Filename (Item : Directory_Entry_Type)
      is
         function Is_Number (X : String) return Boolean
         is (for all C of X => Ada.Characters.Handling.Is_Decimal_Digit (C));

         Name : constant String := Simple_Name (Item);
      begin
         if Name'Length < Head_Length + Tail_Length + 1 then
            Put_Line (Standard_Error,
                      "WARNING: Ignoring too short filename '" & Name & "'");
            return;
         end if;

         declare
            Num : constant String :=
                    Name (Name'First + Head_Length .. Name'Last - Tail_Length);

            Idx : Index_Type;
         begin
            if Is_Number (Num) then
               Idx := Value (Num);

               Min_Index := Min (Idx, Min_Index);
               Max_Index := Max (Idx, Max_Index);
            end if;
         end;
      end Parse_Filename;
   begin
      Search (Directory => Searched_Dir,
              Pattern   => Pattern,
              Filter    => (Ordinary_File => True, others => False),
              Process   => Parse_Filename'Access);

      if Max_Index = Min_Index then
         raise Constraint_Error with "not enough files";
      end if;

      First_Filename :=
        To_Unbounded_String (Full_Radix)
        & "-"
        & Min_Index.Image
        & "." & Extension;

      Last_Filename :=
        To_Unbounded_String (Full_Radix)
        & "-"
        & Max_Index.Image
        & "." & Extension;

      if Config.Debug then
         Put_Line ("1 [" & To_String (First_Filename) & "]");
         Put_Line ("2 [" & To_String (Last_Filename) & "]");
      end if;
   end Filenames_From_Radix;
   ----------------
   -- Initialize --
   ----------------

   procedure Initialize is
      use Ada.Command_Line;
      Output_Dir : Unbounded_String := Null_Unbounded_String;

      Current_Arg : Positive := 1;

      function Remaining_Arg return Natural
      is (Argument_Count - Current_Arg + 1);

      function Next_Argument return String
      is
      begin
         Current_Arg := Current_Arg + 1;
         return Argument (Current_Arg - 1);
      end Next_Argument;

      function Next_Argument return Unbounded_String
      is (To_Unbounded_String (Next_Argument));


      function Peek return String
      is (Argument (Current_Arg));

      procedure Skip_Argument
      is
      begin
         Current_Arg := Current_Arg + 1;
      end Skip_Argument;

      function Is_End_Of_Options (X : String) return Boolean
      is (X = "--");

      function Is_Option (X : String) return Boolean
      is (X'Length > 2
          and then X (X'First .. X'First + 1) = "--");

      procedure Process_Option (Option : String)
        with Pre => Is_Option (Option);

      procedure Process_Option (Option : String)
      is
         Equal_Pos : constant Natural := Index (Option, "=");
         Has_Value : constant Boolean := Equal_Pos /= 0;

         Name      : constant String :=
                       (if Has_Value
                        then
                           Option (Option'First + 2 .. Equal_Pos - 1)
                        else
                           Option (Option'First + 2 .. Option'Last));

         Value     : constant String :=
                       (if Has_Value
                        then
                           Option (Equal_Pos + 1 .. Option'Last)
                        else
                           "");
      begin
         if Name = "output-dir" or Name = "d" then
            if not Has_Value then
               raise Bad_Command_Line with "output-dir expects a value";
            end if;

            Output_Dir := To_Unbounded_String (Value);

         elsif Name = "id" then
            if not Has_Value then
               raise Bad_Command_Line with "id expects a value";
            end if;

            Parse_Object_Ids (Value);

         elsif Name = "verbose" or Name = "V" then
            if Has_Value then
               raise Bad_Command_Line with "Verbose does not need a value";
            end if;

            Verbose_Flag := True;

         else
            Die ("Unknown option '" & Name & "'");
         end if;
      end Process_Option;
   begin
      while Remaining_Arg > 0 and then Is_Option (Peek) loop
         Process_Option (Next_Argument);
      end loop;

      if Is_End_Of_Options (Peek) then
         Skip_Argument;
      end if;

      case Remaining_Arg is
         when 1 =>
            Filenames_From_Radix (Next_Argument);

         when 2 =>
            First_Filename := Next_Argument;
            Last_Filename := Next_Argument;

         when others =>
            Die ("Usage: first last  or radix");
      end case;

      Create_Pattern (From       => First_Filename,
                      To         => Last_Filename,
                      Head       => Output_Pattern_Head,
                      Tail       => Output_Pattern_Tail,
                      Field_Size => Frame_Number_Field_Size,
                      First_Frame => First_Frame_Number,
                      Last_Frame => Last_Frame_Number);

      if Output_Dir /= Null_Unbounded_String then
         declare
            Name         : constant String := To_String (Output_Pattern_Head);
            Full_Pattern : constant String :=
                             Ada.Directories.Compose
                               (Containing_Directory =>
                                                   To_String (Output_Dir),
                                Name                 =>
                                  Ada.Directories.Simple_Name (Name));
         begin
            Output_Pattern_Head := To_Unbounded_String (Full_Pattern);
         end;
      end if;

      if Verbose then
         Put_Line ("using head=" & To_String (Output_Pattern_Head));
      end if;


      --        for I in Object_IDs'First .. Object_Number - 1 loop
      --           Put_Line (Standard_Error, I'Img & "->" & To_String (Object_IDs (I)));
      --        end loop;

      Object_ID_Label := To_Unbounded_String ("id");
   end Initialize;

   --------------------------
   -- First_Frame_Filename --
   --------------------------

   function First_Frame_Filename return String is
   begin
      return To_String (First_Filename);
   end First_Frame_Filename;

   -------------------------
   -- Last_Frame_Filename --
   -------------------------

   function Last_Frame_Filename return String is
   begin
      return To_String (Last_Filename);
   end Last_Frame_Filename;

   ------------------------
   -- Nth_Frame_Filename --
   ------------------------

   function Nth_Frame_Filename (N : Frame_Index) return String is
      function Format (N : Frame_Index) return String is

         Tmp : constant String := Trim (Frame_Index'Image (N), Ada.Strings.Both);
      begin
         if Frame_Number_Field_Size = Variable_Size then
            return Tmp;
         else
            if Tmp'Length > Frame_Number_Field_Size then
               return Tmp;
            else
               return ((Frame_Number_Field_Size - Tmp'Length) * '0') & Tmp;
            end if;
         end if;
      end Format;
   begin
      return To_String (Output_Pattern_Head)
        & Format (N)
        & To_String (Output_Pattern_Tail);
   end Nth_Frame_Filename;

   ---------------
   -- N_Objects --
   ---------------

   function N_Objects return Natural is
   begin
      return Natural (Object_IDs.Length);
   end N_Objects;

   --------------
   -- Id_Label --
   --------------

   function Id_Label return Attribute_Name_Type is
   begin
      return Attribute_Name_Type (To_String (Object_ID_Label));
   end Id_Label;

   -------------------
   -- Nth_Object_Id --
   -------------------

   function Nth_Object_Id (N : Object_Index) return Object_ID_Type is
   begin
      return Object_IDs (N);
   end Nth_Object_Id;

   function Keep_Clip_Still (N : Object_Index) return Boolean
   is (True);


   function First_Frame_Index return Frame_Index is
   begin
      return First_Frame_Number;
   end First_Frame_Index;

   function Last_Frame_Index return Frame_Index is
   begin
      return Last_Frame_Number;
   end Last_Frame_Index;

   function Help_Line return String
   is
   begin
      return
        "Usage: [options] first-frame last-frame object-id(s)" & ASCII.LF
        & ASCII.LF
        & "Accepted options:" & ASCII.LF
        & ASCII.LF
        & "  --verbose (alias: --V)"
        & ASCII.LF
        & "  --output-dir=<output directory> (alias: --d)";
   end Help_Line;

   --------------------
   -- Set_Object_Ids --
   --------------------

   procedure Set_Object_Ids (Names : Object_ID_Vectors.Vector)

   is
   begin
      if N_Objects > 0 then
         raise Program_Error with "Double ID object setting";
      end if;

      Object_IDs := Names;
   end Set_Object_Ids;

   function First_Object_Index return Object_Index
   is (Object_IDs.First_Index);

   function Last_Object_Index return Object_Index
   is (Object_IDs.Last_Index);

end SVG_Animator.Config;

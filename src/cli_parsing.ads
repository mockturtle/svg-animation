with Ada.Containers.Indefinite_Ordered_Sets;
with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;

package CLI_Parsing is
   function Remaining_Arg return Natural;

   function Next_Argument return String;

   function Next_Argument return Unbounded_String;

   function Peek return String;

   procedure Skip_Argument;

   function Is_End_Of_Options (X : String) return Boolean;

   function Is_Option (X : String) return Boolean;

   generic
      type Option_Type is (<>);
   package Option_Processing is
      type Option_Descriptor is private;

      type Parameter_Kind is (Mandatory, Optional, Prohibited);

      function Parse (Item : String;
                      Kind : Parameter_Kind)return Option_Descriptor;

      type Descriptor_Array is array (Option_Type) of Option_Descriptor;

      type Callback_Access is
        access procedure (Option        : Option_Type;
                          Has_Parameter : Boolean;
                          Name          : String;
                          Value         : String);

      procedure Process (Options  : Descriptor_Array;
                         Callback : Callback_Access);
   private
      type Option_Name is new String;

      package Option_Name_Sets is
        new Ada.Containers.Indefinite_Ordered_Sets (Option_Name);

      type Option_Descriptor is
         record
            Set  : Option_Name_Sets.Set;
            Kind : Parameter_Kind;
         end record;
   end Option_Processing;



end CLI_Parsing;

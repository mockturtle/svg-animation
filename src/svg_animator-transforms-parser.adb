with Ada.Strings.Unbounded;          use Ada.Strings.Unbounded;
with SVG_Animator.Transforms.Scanner;
with Ada.Text_IO; use Ada.Text_IO;
pragma Warnings (Off, Ada.Text_IO);

package body SVG_Animator.Transforms.Parser is
   function "+" (X : String) return Unbounded_String
     renames To_Unbounded_String;

   type Descriptor_Type is
      record
         Name : Unbounded_String;
         Min_Param : Natural;
         Max_Param : Natural;
      end record;

   type Descriptor_Array is array (Transforms_Class) of Descriptor_Type;

   Descriptors : constant Descriptor_Array :=
                   (Rotation    => (Name => +"rotate",
                                    Min_Param => 1,
                                    Max_Param => 3),
                    Translation => (Name      => +"translate",
                                    Min_Param => 1,
                                    Max_Param => 2),
                    Scale       => (Name      => +"scale",
                                    Min_Param => 1,
                                    Max_Param => 2),
                    Matrix      => (Name      => +"matrix",
                                    Min_Param => 6,
                                    Max_Param => 6));


   -----------
   -- Parse --
   -----------

   function Parse (Input : String) return Transforms_Descriptor is
      use type Scanner.Token_Class;

      Parameters   : array (1 .. 6) of SVG_Float;
      N_Parameters : Natural := 0;
      Class        : Transforms_Class;

      procedure Add_Parameter (X : SVG_Float) is
      begin
         if N_Parameters = Parameters'Length then
            raise Parsing_Error;
         end if;

         Parameters (N_Parameters + Parameters'First) := X;
         N_Parameters := N_Parameters + 1;
      end Add_Parameter;

      function Make_Full_Transform return Full_Transform
        with Pre => N_Parameters = 6
      is
      begin
         return (Linear => (1 => (Parameters (1), Parameters (3)),
                            2 => (Parameters (2), Parameters (4))),
                 Trans  => Point (Parameters (5 .. 6)));
      end Make_Full_Transform;
   begin
      Scanner.Init (Input);

      if Scanner.Peek_Class /= Scanner.Id then
         raise Parsing_Error;
      end if;

      declare
         Name : constant String := To_String (Scanner.Next_Token.Name);
      begin
         if Scanner.Next_Token.Class /= Scanner.Open then
            raise Parsing_Error;
         end if;

         loop
            if Scanner.Peek_Class /= Scanner.Number then
               raise Parsing_Error;
            end if;

            Add_Parameter (Scanner.Next_Token.Value);

            case Scanner.Next_Token.Class is
            when Scanner.Number | Scanner.Id | Scanner.Open | Scanner.EOF =>
               raise Parsing_Error;

            when Scanner.Comma =>
               null;

            when Scanner.Close=>
               exit;
            end case;
         end loop;

         if Scanner.Peek_Class /= Scanner.EOF then
            raise Parsing_Error;
         end if;

         declare
            Found : Boolean;
         begin
            Found  := False;

--              Put_Line (Standard_Error, "searching for '" & Name & "'");

            for C in Transforms_Class loop
--                 Put_Line (Standard_Error, "proviamo '" & To_String(Descriptors(C).Name) & "'");
               if Descriptors (C).Name = Name then
                  Found := True;
                  Class := C;
                  exit;
               end if;
            end loop;

            if not Found then
               raise Parsing_Error;
            end if;
         end;

         case Class is
            when Rotation =>
               if N_Parameters = 1 then
                  return (Class => Rotation,
                          Angle => Degree (Parameters (1)),
                          Center => (0.0, 0.0));

               elsif N_Parameters = 3 then
                  return (Class => Rotation,
                          Angle => Degree (Parameters (1)),
                          Center => Point (Parameters (2 .. 3)));

               else
                  raise Parsing_Error;
               end if;

            when Translation =>
               if N_Parameters = 1 then
                  return (Class => Translation,
                          X     => Coordinate (Parameters (1)),
                          Y     => 0.0);

               elsif N_Parameters = 2 then
                  return (Class => Translation,
                          X     => Coordinate (Parameters (1)),
                          Y     => Coordinate (Parameters (2)));

               else
                  raise Parsing_Error;
               end if;


            when Scale =>
               if N_Parameters = 1 then
                  return (Class => Scale,
                          Rho_X => Parameters (1),
                          Rho_Y => Parameters (1));

               elsif N_Parameters = 2 then
                  return (Class => Scale,
                          Rho_X => Parameters (1),
                          Rho_Y => Parameters (2));

               else
                  raise Program_Error;
               end if;

            when Matrix =>
               pragma Assert (N_Parameters = 6);

               return (Class => Matrix,
                       Data  => make_Full_Transform);
         end case;
      end;
   end Parse;

end SVG_Animator.Transforms.Parser;

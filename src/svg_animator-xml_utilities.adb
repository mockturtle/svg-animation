with Input_Sources.File; use Input_Sources.File;
with Sax.Readers; use Sax.Readers;
with DOM.Readers; use DOM.Readers;
with DOM.Core.Documents; use DOM.Core.Documents;
with DOM.Core.Nodes; use DOM.Core.Nodes;
with DOM.Core.Elements; use DOM.Core.Elements;

with SVG_Animator.Transforms;          use SVG_Animator.Transforms;
with Ada.Text_IO; use Ada.Text_IO;
pragma Warnings (Off, Ada.Text_IO);

with Ada.Streams.Stream_IO;            use Ada.Streams;

package body SVG_Animator.XML_Utilities is

   type On_Missing_Action is (Die, Return_Null);

   function Find_Node (Doc        : Document;
                       Tag_Name   : String;
                       Tag_Value  : String;
                       On_Missing : On_Missing_Action := Die)
                       return Node
   is
      List   : Node_List;
      N      : Node;
      Result : Node;
   begin
      Result := null;
      List := Get_Elements_By_Tag_Name (Doc      => Doc,
                                        Tag_Name => "*");

      for I in 1 .. Length (List) loop
         N := Item (List, I - 1);

         if Tag_Value = Get_Attribute (N, Tag_Name) then
            if Config.Verbose then
               Put_Line (Standard_Error,
                         "found value '" & Get_Attribute (N, Tag_Name)
                         & "' (" & Tag_Value & ")"
                         & " on name '" &  Tag_Name & "'");
            end if;

            if Result /= null then
               raise Duplicated_ID with Tag_Value;
            else
               Result := N;
            end if;
         end if;
      end loop;

      if Result = null then
         case On_Missing is
            when Die =>
               raise Node_Not_Found with Tag_Value;

            when Return_Null =>
               return null;
         end case;
      end if;

      return Result;
   end Find_Node;

   function Exists (Frame : SVG_File;
                    ID    : Config.Object_ID_Type;
                    Label : Config.Attribute_Name_Type := "id")
                    return Boolean
   is (Find_Node (Doc => Frame.Doc,
                  Tag_Name  => String (label),
                  Tag_Value  => String (id),
                  On_Missing => Return_Null) /= null);


   function Get_Root_Attribute (Frame     : SVG_File;
                                Attribute : String)
                                return String
   is
      Root : constant Node := Frame.Doc;
      Root_Children : constant Node_List := Child_Nodes (Root);
      N : Node;
   begin
      for Idx in 0 .. Length (Root_Children)-1 loop
         N := Item (Root_Children, Idx);

         if Node_Type (N) = Element_Node then
            return Get_Attribute (Elem => n,
                                  Name => Attribute);
         end if;
      end loop;

      raise Program_Error with "No root element?!? This should not happen";
   end Get_Root_Attribute;

   function Load (Filename : String) return SVG_File is
      Input  : File_Input;
      Reader : Tree_Reader;
   begin
      Set_Public_Id (Input, "Preferences file");
      Open (Filename, Input);

      Set_Feature (Reader, Validation_Feature, False);
      Set_Feature (Reader, Namespace_Feature, False);

      Parse (Reader, Input);
      Close (Input);

      return SVG_File'(Reader => Reader,
                       Doc    => Get_Tree (Reader));
   end Load;

   ----------
   -- Save --
   ----------

   procedure Save (Item : SVG_File; Filename : String) is
      Output : Stream_IO.File_Type;
   begin
      Stream_IO.Create (File => Output,
                        Mode => Stream_IO.Out_File,
                        Name => Filename);

      Nodes.Write (Stream_IO.Stream (Output), Item.Doc);

      Stream_IO.Close (Output);
   end Save;

   -------------------
   -- Get_Transform --
   -------------------

   function Get_Transform
     (Frame : SVG_File;
      ID    : Config.Object_ID_Type;
      Label : Config.Attribute_Name_Type)
      return Transforms.SVG_Transform
   is
      N : Node;
   begin
      N := Find_Node (Doc       => Frame.Doc,
                      Tag_Name  => String (Label),
                      Tag_Value => String (ID));

      if Get_Attribute (N, "transform") /= "" then
         --           Put_Line (Standard_Error, "has a transform");
         return Value (Get_Attribute (N, "transform"));
      else
         --           Put_Line (Standard_Error, "has not a transform");
         return Identity;
      end if;
   end Get_Transform;

   -------------------
   -- Set_Transform --
   -------------------

   procedure Set_Transform
     (Frame :  SVG_File;
      ID    : Config.Object_ID_Type;
      Label : Config.Attribute_Name_Type;
      Value : Transforms.SVG_Transform)
   is
      N : Node;
   begin
      N := Find_Node (Doc       => Frame.Doc,
                      Tag_Name  => String (Label),
                      Tag_Value => String (ID));



      Set_Attribute (N, "transform", Image (Value));
   end Set_Transform;

   function Clip_Path_Id (Frame : SVG_File;
                          ID    : Config.Object_ID_Type;
                          Label : Config.Attribute_Name_Type)
                          return Config.Object_ID_Type
   is
      function Parse_Local_Url (X : String)
                                return Config.Object_ID_Type
        with Pre => X'Length >= 7,
          Post => Parse_Local_Url'Result /= Config.No_ID;

      function Parse_Local_Url (X : String)
                                return Config.Object_ID_Type
      is
      begin
         if
           X'Length < 7 or else
           X (X'Last) /= ')'  or else
           X (X'First .. X'First + 4) /= "url(#"
         then
            raise Bad_Clip_Path_Url with X;
         end if;

         return Config.Object_ID_Type (X (X'First + 5 .. X'Last - 1));
      end Parse_Local_Url;

      N : Node;
   begin
      N := Find_Node (Doc       => Frame.Doc,
                      Tag_Name  => String (Label),
                      Tag_Value => String (ID));

      declare
         Clip_Id : constant String := Get_Attribute (N, "clip-path");
      begin
         if Clip_Id = "none" or Clip_Id = "" then
            return Config.No_ID;

         else
            --           Put_Line (Standard_Error, "has a transform");
            --  Put_Line ("<<" & Get_Attribute (N, "clip-path") & ">>");
            return Parse_Local_Url (Get_Attribute (N, "clip-path"));
         end if;
      end;
   end Clip_Path_Id;


end SVG_Animator.XML_Utilities;

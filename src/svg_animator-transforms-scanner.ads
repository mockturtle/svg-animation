with Ada.Strings.Unbounded;   use Ada.Strings.Unbounded;

private
package SVG_Animator.Transforms.Scanner is
   type Token_Class is (Id, Number, Open, Close, Comma, EOF);

   type Token_Type (Class : Token_Class) is
      record
         case Class is
            when Id =>
               Name : Unbounded_String;

            when Number =>
               Value : SVG_Float;

            when Open | Close | Comma | EOF =>
               null;
         end case;
      end record;

   procedure Init (Input : String);

   function Next_Token return Token_Type;

   function Peek_Class return Token_Class;

end SVG_Animator.Transforms.Scanner;

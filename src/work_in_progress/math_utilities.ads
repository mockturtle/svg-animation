with Ada.Numerics.Generic_Real_Arrays;
with Ada.Numerics.Generic_Elementary_Functions;

generic
   with package Matrices
     is new Ada.Numerics.Generic_Real_Arrays (Real => <>);
package Math_Utilities is
   use Matrices;

   package Elementary is
     new Ada.Numerics.Generic_Elementary_Functions (Real);

   Threshold : constant Matrices.Real;

   function "**" (X : Matrices.Real_Matrix;
                  N : Positive)
                  return Matrices.Real_Matrix;

   function Is_Square (X : Matrices.Real_Matrix) return Boolean
   is (X'Length (1) = X'Length (2));

   function Has_Same_Shape (X, Y : Matrices.Real_Matrix) return Boolean
   is (X'Length (1) = Y'Length (1) and X'Length (2) = Y'Length (2));

   function Frobenius_Norm (X : Matrices.Real_Matrix) return Matrices.Real
     with Pre => Is_Square (X);

   function Is_Admissible (X : Matrices.Real_Matrix) return Boolean
     with Pre => X'Length (1) = 2  and X'Length (2) = 2;

   function Trace (X : Matrices.Real_Matrix) return Matrices.Real
     with Pre => Is_Square (X);

   function Cond (X : Matrices.Real_Matrix) return Matrices.Real
     with Pre => Is_Square (X);

   function Is_Approx_Equal (X, Y : Matrices.Real_Matrix;
                             Tol  : Matrices.Real := Threshold) return Boolean;

   function Is_Approx_Equal (X, Y : Matrices.Real_Matrix;
                             Tol  : Matrices.Real := Threshold) return Boolean
   is (Has_Same_Shape (X, Y) and then Frobenius_Norm (X - Y) <= Tol);

   function Is_Upper_Triangular (X    : Matrices.Real_Matrix;
                                 Tol  : Matrices.Real := Threshold)
                                 return Boolean
   is (for all Row in X'Range (1) =>
         (for all Col in X'First (2) .. Row - 1 =>
              (abs (X (Row, Col)) <= Tol)));


   function Nth_Root (X : Matrices.Real_Matrix;
                      N : Positive)
                      return Matrices.Real_Matrix
     with
       Pre =>
         Is_Square(X)
     and X'Length (1) = 2
     and Is_Admissible (X),
     Post =>
       Is_Approx_Equal (Nth_Root'Result ** N, -  X);

   procedure QR (X : Matrices.Real_Matrix;
                 Q : out Matrices.Real_Matrix;
                 R : out Matrices.Real_Matrix)
     with
       Pre =>
         Is_Square (X)
     and Has_Same_Shape (X, Q)
     and Has_Same_Shape (X, R),
     Post =>
       Is_Upper_Triangular (R)
     and (for all I in R'Range(1) => R(I,I) >= 0.0)
     and Is_Approx_Equal (Transpose (Q) * Q, Unit_Matrix (Q'Length (1)))
     and Is_Approx_Equal (X, Q * R);

   function Norm (X : Real_Vector) return Real
   is (Elementary.Sqrt (X * X));

   function Block_Diagonal (X, Y : Real_Matrix) return Real_Matrix;

   type Index_Range is
      record
         Low, High : Integer;
      end record;

   Inf : constant Integer := Integer'Last;

   function Submatrix (X   : Real_Matrix;
                       Row : Index_Range;
                       Col : Index_Range)
                       return Real_Matrix;

   function Submatrix (X   : Real_Matrix;
                       Row : Integer;
                       Col : Index_Range)
                       return Real_Matrix
   is (Submatrix (X, (Row, Row), Col));

   function Submatrix (X   : Real_Matrix;
                       Row : Index_Range;
                       Col : Integer)
                       return Real_Matrix
   is (Submatrix (X, Row, (Col, Col)));

   function Submatrix (X   : Real_Matrix;
                       Row : Integer;
                       Col : Integer)
                       return Real_Matrix
   is (Submatrix (X, (Row, Row), (Col, Col)));

   function Submatrix (X   : Real_Matrix;
                       Row : Index_Range;
                       Col : Integer)
                       return Real_Vector;

private
   use Matrices;

   Threshold : constant Matrices.Real := 1.0e-6;

   function Is_Scalar (X : Matrices.Real_Matrix) return Boolean
   is (for all Row in X'Range (1) =>
         (for all Col in X'Range (2) =>
              (if Row = Col then
                    abs (X (Row, Row)- X (1, 1)) <= Threshold
               else
                  abs (X (Row, Col)) <= Threshold)
         )
      );


   function Is_Admissible (X : Matrices.Real_Matrix) return Boolean
   is (
       Is_Square (X) and then
         (
          (Determinant (X) > Trace (X) ** 2)                  -- Complex eigenvalues
          or else (Determinant (X) > 0.0 and Trace (X) > 0.0)     -- Positive eigenvalues
          or else (Is_Scalar (X))
         )
      );

end Math_Utilities;

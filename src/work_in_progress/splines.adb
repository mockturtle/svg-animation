with Ada.Numerics.Generic_Real_Arrays;
with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;
with Ada.Text_IO; use Ada.Text_IO;

package body Splines is
   package Matrices is
     new Ada.Numerics.Generic_Real_Arrays (Real);

   ----------------
   -- Get_Spline --
   ----------------

   function Get_Spline
     (X       : Real_Array;
      Y       : Real_Array;
      Initial : Initial_Conditions;
      Order   : Spline_Degree := 3)
      return Spline_Type
   is
      use Matrices;

      N_Polynomials : constant Polynomial_Index := Polynomial_Index (X'Length - 1);
      N_Variables   : constant Positive := Natural (Order + 1) * Natural (N_Polynomials);

      subtype Variable_Index is Integer range 1 .. N_Variables;

      Nodes  : constant Extreme_Array (0 .. Extreme_Index (X'Length - 1)) := Extreme_Array (X);
      Values : constant Extreme_Array (0 .. Extreme_Index (Y'Length - 1)) := Extreme_Array (Y);

      Result : Spline_Type := (Size              => N_Polynomials,
                               Interval_Extremes => Extreme_Array (X),
                               Polynomials       => <>);


      Mtx    : Real_Matrix (Variable_Index, Variable_Index) :=
                 (others => (others => 0.0));

      Vec    : Real_Vector (Variable_Index);
      Coeff  : Real_Vector (Variable_Index);

      type Row_Buffer is new Real_Vector (Variable_Index);

      Current_Row    : Positive := Mtx'First;

      procedure Append_Row (Coeff : Row_Buffer; Val : Real) is
      begin
         Put_Line ("Row :" & Current_Row'img);

         for I in Mtx'Range (2) loop
            Mtx (Current_Row, I) := Coeff (I);

            if Coeff (I) /= 0.0 then
               Put_Line (I'Img & "->" & Real'Image(Coeff(I)));
            end if;
         end loop;

         Vec (Current_Row) := Val;
         Current_Row := Current_Row + 1;
      end Append_Row;

      function Position_Of (Pol : Polynomial_Index;
                            Deg : Spline_Degree)
                            return Positive
      is (Variable_Index'First
          + Integer (Deg)
          + Natural (Order + 1) * Natural (Pol - Polynomial_Index'First));
      -- Return the position in the column vector that corresponds to the
      -- coefficient of degree Deg of polynomial Pol

      procedure Write_Derivative_Eq (Pol_1 : Polynomial_Index;
                                     Pol_2 : Polynomial_Index;
                                     X     : Real;
                                     Der   : Derivative_Order);
      -- Append to the system the equation that represents the
      -- equality in X of the derivatives of order Der in of Pol_1 and Pol_2

      procedure Write_Derivative_Eq (Pol   : Polynomial_Index;
                                     X     : Real;
                                     Value : Real;
                                     Der   : Derivative_Order);
      -- Append to the system the equation that represents the fact that
      -- in X the derivative of order Der in of Pol is equal to Value

      procedure Write_Value_Eq (Pol : Polynomial_Index;
                                X   : Real;
                                Y   : Real);
      -- Append to the system the equation that represents the fact that
      -- in X the value of Pol is equal to Y


      procedure Write_Value_Eq (Pol : Polynomial_Index;
                                X   : Real;
                                Y   : Real)
      is
         Buffer : Row_Buffer := (others => 0.0);
      begin
         Put_Line ("Value eq" & Pol'Img & X'Img & Y'Img);

         --
         -- The equation to be written is
         --
         --     a_0 + a_1 X + a_2 X^2 + ... a_ord X^ord = Y
         --
         -- Therefore, the coefficient of a_i is X**i
         --
         for Deg in 0 .. Order loop
            Buffer (Position_Of (Pol, Deg)) := X ** Natural (Deg);
         end loop;

         Append_Row (Buffer, Y);
      end Write_Value_Eq;

      function Derivative_Coeff (Deg : Spline_Degree;
                                 Der : Derivative_Order;
                                 X   : Real)
                                 return Real;
      --
      -- Return the coefficient of a_deg in the derivative of
      -- order Der computed in X
      --


      function Derivative_Coeff (Deg : Spline_Degree;
                                 Der : Derivative_Order;
                                 X   : Real) return Real
      is
         Tmp : Real;
      begin
         --
         -- If P(x) = \sum a_i x^i, the value that multiplies
         -- a_i in P^(d)(x) (the d-th derivative) is
         --
         --   0                                    if i < d
         --   i*(i-1)* .. * (i-d+1) * x^(i-d)      if d >= i
         --
         if Deg < Spline_Degree(Der) then
            Tmp := 0.0;
         else
            declare
               D : constant Natural := Natural (Deg) - Natural (Der);
            begin
               Tmp := X ** D;

               for K in (D + 1) .. Natural(Deg) loop
                  Tmp := Tmp * Real (K);
               end loop;
            end;
         end if;

         return Tmp;
      end Derivative_Coeff;




      procedure Write_Derivative_Eq (Pol   : Polynomial_Index;
                                     X     : Real;
                                     Value : Real;
                                     Der   : Derivative_Order)

      is
         Tmp : Real;
         Buffer : Row_Buffer := (others => 0.0);
      begin
         for Deg in 0 .. Order loop
            Tmp := Derivative_Coeff (Deg => Deg,
                                     Der => Der,
                                     X   => x);

            Buffer (Position_Of (Pol, Deg)) := Tmp;
         end loop;

         Append_Row (Buffer, Value);
      end Write_Derivative_Eq;


      procedure Write_Derivative_Eq (Pol_1 : Polynomial_Index;
                                     Pol_2 : Polynomial_Index;
                                     X     : Real;
                                     Der   : Derivative_Order)
      is
         Tmp : Real;
         Buffer : Row_Buffer := (others => 0.0);
      begin
         for Deg in 0 .. Order loop
            Tmp := Derivative_Coeff (Deg => deg,
                                     Der => Der,
                                     X   => x);

            Buffer (Position_Of (Pol_1, Deg)) := Tmp;
            Buffer (Position_Of (Pol_2, Deg)) := -Tmp;
         end loop;

         Append_Row (Buffer, 0.0);
      end Write_Derivative_Eq;
   begin
      for Pol in 1 .. N_Polynomials loop
         Write_Value_Eq (Pol, Nodes (Lo_Extreme (Pol)), Values (Lo_Extreme (Pol)));
         Write_Value_Eq (Pol, Nodes (Hi_Extreme (Pol)), Values (Hi_Extreme (Pol)));
      end loop;

      for Pol in 1 .. N_Polynomials - 1 loop
         for Derivative in 1 .. Derivative_Order (Order - 1) loop
            Write_Derivative_Eq (Pol_1 => Pol,
                                 Pol_2 => Pol + 1,
                                 X     => Nodes (Hi_Extreme (Pol)),
                                 Der   => Derivative);
         end loop;
      end loop;

      for Derivative in Initial.Left'Range loop
         Write_Derivative_Eq (Pol   => 1,
                              X     => Nodes (Lo_Extreme (1)),
                              Value => Initial.Left (Derivative),
                              Der   => Derivative);

         Write_Derivative_Eq (Pol   => N_Polynomials,
                              X     => Nodes (Hi_Extreme (N_Polynomials)),
                              Value => Initial.Right (Derivative),
                              Der   => Derivative);
      end loop;


--        Put_Line ("XXXX" & Current_Row'Img & N_Variables'Img);
      pragma Assert (Current_Row = N_Variables + 1);

      Coeff := Inverse (Mtx) * Vec;

      for Pol in 1 .. N_Polynomials loop
         Result.Polynomials (Pol).Degree := Order;

         for Deg in 0 .. Order loop
            Result.Polynomials (Pol).Coefficients (Deg) :=
              Coeff (Position_Of (Pol, Deg));
         end loop;
      end loop;

      return Result;
   end Get_Spline;

   function Get_Spline (X     : Real_Array;
                        Y     : Real_Array;
                        Order : Spline_Degree := 3)
                        return Spline_Type
   is
      Initial : constant Initial_Conditions :=
                  (Max_Derivative => Derivative_Order (Order - 1) / 2,
                   Left           => (others => 0.0),
                   Right          => (others => 0.0));

   begin
      return Get_Spline (X       => X,
                         Y       => Y,
                         Initial => Initial,
                         Order   => Order);
   end Get_Spline;


   -----------------
   -- Get_Segment --
   -----------------

   function Get_Segment
     (Spline   : Spline_Type;
      Position : Real)
      return Polynomial
   is
   begin
      if Position <= Spline.Interval_Extremes (0) then
         return Spline.Polynomials (1);

      elsif Position >= Spline.Interval_Extremes (Spline.Size) then
         return Spline.Polynomials (Spline.Size);

      else
         declare
            Hi  : Extreme_Index := Hi_Extreme (Spline.Polynomials'Last);
            Lo  : Extreme_Index := Lo_Extreme (Spline.Polynomials'First);
            Mid : Extreme_Index;
         begin
            pragma Assert (Hi = Spline.Interval_Extremes'Last);
            pragma Assert (Lo = Spline.Interval_Extremes'First);
            pragma Assert (Position > Spline.Interval_Extremes (Lo));
            pragma Assert (Position < Spline.Interval_Extremes (Hi));

            while Hi > Lo + 1 loop
               Mid := (Hi + Lo) / 2;

               if Position = Spline.Interval_Extremes (Mid) then
                  Hi := Mid;
                  Lo := Mid - 1;

               elsif Position < Spline.Interval_Extremes (Mid) then
                  Hi := Mid;

               else
                  Lo := Mid;
               end if;
            end loop;

            return Spline.Polynomials (Hi);
         end;
      end if;
   end Get_Segment;

   ----------
   -- Eval --
   ----------

   function Eval
     (P : Polynomial;
      X : Real)
      return Real
   is
      Result : Real := 0.0;
   begin
      for I in reverse 0 .. P.Degree loop
         Result := X * Result + P.Coefficients (I);
      end loop;

      return Result;
   end Eval;


   function Image (P   : Polynomial;
                   Var : String := "x") return String
   is
      Result : Unbounded_String := Null_Unbounded_String;

      procedure Append (To    : in out Unbounded_String;
                        Coeff : Real;
                        Deg   : Spline_Degree)
      is
         S : constant String :=
               (if Coeff = 1.0 then
                  (if Deg = 0 then "1" else "")
                elsif Coeff = -1.0 then
                  (if Deg = 0 then "-1" else "")
                else
                   Real'Image (Coeff))
               & (if Deg = 0 then
                         ""
                  elsif Deg = 1 then
                     Var
                  else
                     Var & "^" & Spline_Degree'Image (Deg));
      begin
         if To = Null_Unbounded_String then
            To := To_Unbounded_String (S);

         else
            if Coeff > 0.0 then
               To := To & "+" & S;

            else
               To := To & S; -- The "-" is already in S
            end if;
         end if;
      end Append;
   begin
      for D in 0 .. P.Degree loop
         if P.Coefficients (D) /= 0.0 then
            Append (Result, P.Coefficients (D), D);
         end if;
      end loop;

      if Result = Null_Unbounded_String then
         return "0.0";
      else
         return To_String (Result);
      end if;
   end Image;

   function Image (S   : Spline_Type;
                   Var : String := "x") return String
   is
      Result : Unbounded_String := Null_Unbounded_String;
   begin
      for I in S.Polynomials'Range loop
         if Result /= Null_Unbounded_String then
            Result := Result & ASCII.LF;
         end if;

         Result := Result
           & Real'Image (S.Interval_Extremes (I - 1))
           & "<= " & Var & " <="
           & Real'Image (S.Interval_Extremes (I))
           & "       "
           & Image (S.Polynomials (I), Var);
      end loop;

      return To_String (Result);
   end Image;

end Splines;

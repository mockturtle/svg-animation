
generic
   type Real is digits <>;
package Splines is
   type Spline_Degree is new Integer range 0 .. 10;

   type Derivative_Order is new Integer range 0 .. 10;

   type Real_Array is array (Natural range <>) of Real;

   type Derivative_Array is array (Derivative_Order range <>) of Real;

   type Initial_Conditions (Max_Derivative : Derivative_Order) is
      record
         Left  : Derivative_Array (1 .. Max_Derivative);
         Right : Derivative_Array (1 .. Max_Derivative);
      end record;


   type Spline_Type (<>) is private;

   function Get_Spline (X     : Real_Array;
                        Y     : Real_Array;
                        Order : Spline_Degree := 3)
                        return Spline_Type
     with
       Pre =>
         X'Length = Y'Length
         and (for all I in X'First .. X'Last - 1 => X (I) < X (I + 1))
         and Order mod 2 = 1
         and Order > 1;

   function Get_Spline (X       : Real_Array;
                        Y       : Real_Array;
                        Initial : Initial_Conditions;
                        Order   : Spline_Degree := 3)
                        return Spline_Type
     with
       Pre =>
         X'Length = Y'Length
         and (for all I in X'First .. X'Last - 1 => X (I) < X (I + 1))
         and (Initial.Max_Derivative = Derivative_Order (Order - 1) / 2)
         and Order mod 2 = 1
         and Order > 1;



   function Eval (Spline : Spline_Type;
                  X      : Real)
                  return Real;


   type Polynomial is private;

   function Get_Segment (Spline   : Spline_Type;
                         Position : Real)
                         return Polynomial;

   function Eval (P : Polynomial;
                  X : Real)
                  return Real;

   function Coefficients (P : Polynomial) return Real_Array;


   function Image (P : Polynomial;
                   Var : String := "x") return String;

   function Image (S : Spline_Type;
                   Var : String := "x") return String;

private

   type Coefficient_Array is array (Spline_Degree) of Real;

   type Polynomial is
      record
         Degree       : Spline_Degree;
         Coefficients : Coefficient_Array;
      end record;

   type Extreme_Index is new Natural;

   subtype Polynomial_Index is Extreme_Index range 1 .. Extreme_Index'Last;


   type Polynomial_Array is
     array (Polynomial_Index range <>) of Polynomial;

   type Extreme_Array is
     array (Extreme_Index range <>) of Real;

   function Lo_Extreme (Pol : Polynomial_Index) return Extreme_Index
   is (Extreme_Index (Pol - 1));

   function Hi_Extreme (Pol : Polynomial_Index) return Extreme_Index
   is (Extreme_Index (Pol));

   type Spline_Type (Size : Extreme_Index) is
      record
         Interval_Extremes : Extreme_Array (0 .. Size);
         Polynomials       : Polynomial_Array (1 .. Size);
      end record;

   function Eval (Spline : Spline_Type;
                  X      : Real)
                  return Real
   is (Eval (Get_Segment (Spline, X), X));

   function Coefficients (P : Polynomial) return Real_Array
   is (Real_Array (P.Coefficients (0 .. P.Degree)));

end Splines;

with Splines;
with Ada.Text_IO; use Ada.Text_IO;

procedure Test_Splines is
   type My_Float is digits 16;

   package SP is new Splines (My_Float);


   X : SP.Real_Array := (-2.0, -1.0, 0.0, 1.0, 2.0);
   Y : SP.Real_Array := (0.0, 1.0/3.0, 1.0, 1.0/3.0, 0.0);


   procedure Check (X, Y : SP.Real_Array) is
      S : SP.Spline_Type := SP.Get_Spline (X     => X,
                                           Y     => Y,
                                           Order => 3);

      D, D1, D2 : My_Float;
      Step : constant My_Float := 1.0e-9;
   begin
      Put_Line (SP.Image (S));

      for I in X'Range loop
         D := abs (Y (I) - SP.Eval (S, X (I)));

         Put_Line ("Err in X="
                   & My_Float'Image (X (I))
                   & "->"
                   & My_Float'Image (D));
      end loop;

      for I in X'First + 1 .. X'Last - 1 loop
         D := abs (SP.Eval (S, X (I)-Step) - SP.Eval (S, X (I)+Step));
         D1 := (SP.Eval (S, X (I)-Step) - SP.Eval (S, X (I))) / Step;
         D2 := (SP.Eval (S, X (I)) - SP.Eval (S, Step + X (I))) / Step;
         Put_Line ("Continuita in X="
                   & My_Float'Image (X (I))
                   & "->"
                   & My_Float'Image (D)
                   & ", derivata="
                   & My_Float'Image (D1)
                   & My_Float'Image (D2)
                   & My_Float'Image (abs(D1-D2)));

      end loop;
   end Check;

begin
   Check (X, Y);
end Test_Splines;

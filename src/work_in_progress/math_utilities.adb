with Ada.Numerics.Generic_Elementary_Functions;
with Ada.Numerics.Generic_Complex_Arrays;
with Ada.Numerics.Generic_Complex_Types;

package body Math_Utilities is
   package Complexes is
     new Ada.Numerics.Generic_Complex_Types (Real => Matrices.Real);

   use Complexes;

   package Complex_Matrices is
     new Ada.Numerics.Generic_Complex_Arrays (Real_Arrays   => Matrices,
                                              Complex_Types => Complexes);

   package Elementary_Functions is
     new Ada.Numerics.Generic_Elementary_Functions (Matrices.Real);

   function Eye (Order   : Positive;
                 First_1 : Integer := 1;
                 First_2 : Integer := 1)
                 return Real_Matrix
                 renames Unit_Matrix;

   ----------
   -- "**" --
   ----------

   function "**"
     (X : Matrices.Real_Matrix;
      N : Positive)
      return Matrices.Real_Matrix
   is
      Result : Matrices.Real_Matrix := Matrices.Unit_Matrix (X'Length (1));
   begin
      if not Is_Square (X) then
         raise Constraint_Error;
      end if;

      for I in 1 .. N loop
         Result := Result * X;
      end loop;

      return Result;
   end "**";

   ----------
   -- Norm --
   ----------

   function Frobenius_Norm (X : Matrices.Real_Matrix) return Matrices.Real is

      Result : Real := 0.0;
   begin
      for Row in X'Range (1) loop
         for Col in X'Range (2) loop
            Result := Result + X (Row, Col) ** 2;
         end loop;
      end loop;

      return Elementary_Functions.Sqrt (Result);
   end Frobenius_Norm;

   function Cond (X : Matrices.Real_Matrix) return Matrices.Real is
   begin
      return Frobenius_Norm (X) * Frobenius_Norm (Inverse (X));
   end Cond;
   -----------
   -- Trace --
   -----------

   function Trace (X : Matrices.Real_Matrix) return Matrices.Real is

      Result : Real := 0.0;
   begin
      if not Is_Square (X) then
         raise Constraint_Error;
      end if;

      for I in X'Range (1) loop
         Result := Result + X (I, I);
      end loop;

      return Result;
   end Trace;

   --------------
   -- Nth_Root --
   --------------

   function Nth_Root
     (X : Matrices.Real_Matrix;
      N : Positive)
      return Matrices.Real_Matrix
   is
   begin
      --  Generated stub: replace with real body!
      pragma Compile_Time_Warning (Standard.True, "Nth_Root unimplemented");
      raise Program_Error with "Unimplemented function Nth_Root";
      return Nth_Root (X, N);
   end Nth_Root;

   procedure QR (X : Matrices.Real_Matrix;
                 Q : out Matrices.Real_Matrix;
                 R : out Matrices.Real_Matrix) is

      function Householder_Symmetry (V : Real_Vector) return Real_Matrix
      is
         Alpha  : constant Real := -Norm (V) * (if V (V'First) > 0.0 then 1.0 else -1.0);
         U      : Real_Vector := V - Alpha * Unit_Vector (1, V'Length);
      begin
         U := U * (1.0 / Norm (U));

         return Unit_Matrix (V'Length) - 2.0 * U * U;
      end Householder_Symmetry;

   begin
      Q := Unit_Matrix (Q'Length (1));
      R := X;

      for Col in X'First (2) .. X'Last (2)-1 loop
--           pragma Loop_Invariant (Q * R = X);

         declare
            Column : constant Real_Vector := Submatrix (R, (Col, Inf), Col);
            Block  : constant Real_Matrix := Householder_Symmetry (Column);
            Q_K    : constant Real_Matrix :=
                       Block_Diagonal
                         (Eye (X'Length (1) - Block'Length (1)), Block);
         begin
            R := Q_K * R;
            Q := Q * Transpose (Q_K);
         end;
      end loop;

      declare
         Signs : Real_Matrix := Unit_Matrix (Q'Length);
      begin
         for Row in R'Range (1) loop
            if R (Row, Row) < 0.0 then
               Signs (Row, Row) := -1.0;
            end if;
         end loop;

         pragma Assert (for all Row in R'Range (1) => R (Row, Row) * Signs (Row, Row) >= 0.0);

         R := Signs * R;
         Q := Q * Signs;

         pragma Assert (for all Row in R'Range (1) => R (Row, Row) >= 0.0);
--           pragma Assert (Q * R = X);
      end;

      -- Clean up the lower part of R

      for Row in R'Range (1) loop
         for Col in R'First (2) .. Row - 1 loop
            R (Row, Col) := 0.0;
         end loop;
      end loop;

   end QR;

   function Submatrix (X   : Real_Matrix;
                       Row : Index_Range;
                       Col : Index_Range)
                       return Real_Matrix
   is
      Last_Row : constant Integer := Integer'Min (Row.High, X'Last (1));
      Last_Col : constant Integer := Integer'Min (Col.High, X'Last (2));

      Result   : Real_Matrix (1 .. Last_Row - Row.Low + 1, 1 .. Last_Col - Col.Low + 1);
   begin
      for R in Result'Range (1) loop
         for c in Result'Range (2) loop
            Result (R, C) := X (R - 1 + X'First (1), C - 1 + X'First (2));
         end loop;
      end loop;

      return Result;
   end Submatrix;

   function Submatrix (X   : Real_Matrix;
                       Row : Index_Range;
                       Col : Integer)
                       return Real_Vector
   is
      Last_Row : constant Integer := Integer'Min (Row.High, X'Last (1));
      Result   : Real_Vector (1 .. Last_Row - Row.Low + 1);
   begin
      for R in Result'Range loop
         Result (R) := X (R - 1 + X'First (1), Col);
      end loop;

      return Result;
   end Submatrix;


   function Block_Diagonal (X, Y : Real_Matrix) return Real_Matrix
   is
      Result : Real_Matrix (1 .. X'Length (1)+y'Length (1),
                            1 .. X'Length (2)+Y'Length (2))
        := (others => (others => 0.0));
   begin
      for R in X'Range (1) loop
         for C in X'Range (2) loop
            Result (R - X'First (1)+1, C - X'First (2)+1) := X (R, C);
         end loop;
      end loop;

      for R in y'Range (1) loop
         for C in y'Range (2) loop
            Result (R - Y'First (1)+X'Length (1)+1, C - Y'First (2)+X'Length (2)+1) := Y (R, C);
         end loop;
      end loop;

      return Result;
   end Block_Diagonal;

end Math_Utilities;
